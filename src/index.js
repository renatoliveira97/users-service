import express from 'express';
import * as yup from 'yup';
import { v4 as uuidv4 } from 'uuid';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const app = express();

app.use(express.json());

let users = [];

const config = {
    secret: "my_secret",
    expiresIn: "1h",
};

const userSchema = yup.object().shape({
    uuid: yup.string().default(() => { return uuidv4()}).transform( () => { return uuidv4();}),
    age: yup.number().positive().required(),
    username: yup.string().required().min(3),
    email: yup.string().required().email(),
    password: yup.string().required()
    .transform( (value, originalValue) => {
        return bcrypt.hashSync(originalValue, 10);
    }),
    createdOn: yup.date().default(() => {return new Date();}).transform(() => {return new Date();})
});

const loginSchema = yup.object().shape({
    username: yup.string().required().min(3),
    password: yup.string().required()
});

const updateSchema = yup.object().shape({
    password: yup.string().required()
    .transform( (value, originalValue) => {
        return bcrypt.hashSync(originalValue, 10);
    })
});

const validate = (schema) => async (req, res, next) => {
    try {
        const validatedData = await schema.validate(req.body, { abortEarly: false, stripUnknown: true});
        req.validatedData = validatedData;
        next();
    } catch (e) {
        return res.status(422).json({ 
            message: e.errors 
        });
    }
}

const isAuthenticated = async (req, res, next) => {
    if (req.headers.authorization === undefined) {
        return res.status(401).json({ 
            error: 'Token not informed!' 
        });
    }
    const token = req.headers.authorization.split(' ')[1];

    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
           return res.status(401).json({ 
               error: 'Invalid Token' 
            });
        }

        req.user = decoded.user;
        next();
    });
}

const userAuthorization = async (req, res, next) => {
    const { uuid } = req.params;
    const user_uuid = req.user.uuid;

    if (uuid !== user_uuid) {
        return res.status(403).json({
            error: "You don't have permission to do that"
        })
    }

    next();
}


app.post('/signup', validate(userSchema), (req, res) => {
    const user = req.validatedData;
    let response_data = Object.assign({}, user);

    users.push(user);
    delete response_data.password;
    
    return res.status(201).json(response_data);
});

app.post('/login', validate(loginSchema), (req, res) => {
    const { username, password } = req.validatedData;
    
    const user = users.find(user => user.username === username);
    
    if (!user) {
        return res.status(401).json({ error: 'User not found!' });
    } else if (!bcrypt.compareSync(password, user.password)) {
        return res.status(401).json({ error: 'Password missmatch' });
    }
    
    const token = jwt.sign(
        { user },
        config.secret,
        {
            expiresIn: config.expiresIn
        }
    );
    
    return res.status(200).json({ token });

});

app.put('/users/:uuid/password', validate(updateSchema), isAuthenticated, userAuthorization, (req, res) => {
    const { password } = req.validatedData;
    const { uuid } = req.params;

    const user = users.find(user => user.uuid === uuid);

    user.password = password;

    return res.status(204).send();
});

app.get('/users', isAuthenticated, (req, res) => {
    return res.status(200).json(users);
})

app.listen(3000, () => {
    console.log('Running at http://localhost:3000');
});
