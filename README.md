<div align="center">
    <h1>Users service</h1>
    <h4>A basic user create update and login system<h4>
    <img src="https://i0.wp.com/projectsplaza.com/wp-content/uploads/2018/02/create-simple-static-website-with-nodejs-express.png?resize=930%2C400&ssl=1" alt="Node and express image">
</div>

## Setup

* Clone the repository & `cd` to the root directory.

* Install the dependencies with the command bellow:

```
yarn install
```

## Run

To run the application with the nodemon in the development environment, you need to use the command bellow:

```
yarn dev
```

The application will run locally in port `3000`.

## API Routes

### POST /signup

This route is used to create a new user.

```
RESPONSE STATUS -> HTTP 201 (CREATED)
```

Body:

```
{
    "age": 18,
    "username": "daniel",
    "email": "daniel@kenzie.com",
    "password": "abcd"
}
```

Response: 

```
{
    "uuid": "4b72c6f3-6d0a-(X)6a1-86c6-687d52de4fc7",
    "createdOn": "2022-01-15T01:23:52.910Z",
    "email": "daniel@kenzie.com",
    "age": 18,
    "username": "daniel"
}
```

### POST /login

Route used to login into the system.

```
RESPONSE STATUS -> HTTP 200 (OK)
```

Body: 

```
{
    "username": "daniel",
    "password": "abcd"
}
```

Response:

```
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI.eyJ1c2VybmFtZSI6Imx1Y2FzIi
              jc4ZGE2N2VhLTMwM2EtNDYxOC1imOWFkZDY1MiIsImlhdCI6MTYzNz
              XhwIjoxNjM3MjAyMjQyfQ._XIs736ET7wEMJ5Ldvcsjqsg4Nvs40mM"
}
```

### GET /users

Route used to list all the users in the system.
* You need an authentication token to use this route.

```
RESPONSE STATUS -> HTTP 200 (OK)
```

Response:

```
[
    {
        "password": "$2b$10$7GIfNyBYboR/FM4dB.
                     Qu.MREt3HkCL2jvAkkAOm.6dyO",
        "uuid": "4b72c6f3-6d0a-(X)6a1-86c6-687d52de4fc7",
        "createdOn": "2022-01-15T01:23:52.910Z",
        "email": "daniel@kenzie.com",
        "age": 18,
        "username": "daniel"
    }
]
```

### PUT /users/:uuid/password

This route is used to update the user password.
* You need an authentication token to use this route.

```
RESPONSE STATUS -> HTTP 204 (NO CONTENT)
```

Body:

```
{
    "password": "0000000"
}
```

## Remarks

This template is developed and tested on

- Node v16.13.1
- Ubuntu 20.04.3 LTS
